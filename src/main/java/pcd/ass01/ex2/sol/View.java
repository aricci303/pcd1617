package pcd.ass01.ex2.sol;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

class View extends JFrame implements ActionListener {

	private JButton start, stop;
	private Controller controller;
	
	public View(Controller controller) {
		super("View");
		this.controller = controller;
		
		setSize(300, 60);
		setResizable(false);
		
		start = new JButton("start");
		start.addActionListener(this);

		stop = new JButton("stop");
		stop.addActionListener(this);
		
		JPanel panel = new JPanel();
		panel.add(start);		
		panel.add(stop);	
		
		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);
	}
	
	public void actionPerformed(ActionEvent ev) {
		try {
			if (ev.getSource() == start){
				start.setEnabled(false);
				controller.notifiedStart();
			} else if (ev.getSource() == stop){
				controller.notifiedStop();
			}
		} catch (Exception ex) {
		}
	}

}
