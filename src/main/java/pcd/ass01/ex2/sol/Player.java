package pcd.ass01.ex2.sol;

import java.util.concurrent.Semaphore;

public class Player extends Thread {

	private Counter counter;
	private Semaphore iCanGo;
	private Semaphore friendCanGo;
	private Semaphore shutdown;
	private StopFlag stop;
	private Output output;
	private String message;
	
	public Player(String message, Counter counter, Semaphore iCanGo, Semaphore friendCanGo, StopFlag stop, Output output, Semaphore shutdown){
		this.message = message;
		this.counter = counter;
		this.iCanGo = iCanGo;
		this.friendCanGo = friendCanGo;
		this.stop = stop;
		this.output = output;
		this.shutdown = shutdown;
	}
	
	public void run(){
		try {
			while (true){
				iCanGo.acquire();
				if (!stop.isSet()){
					counter.inc();
					output.print(message);
					friendCanGo.release();			
				} else {
					counter.close();
					friendCanGo.release();			
					output.print("Goodbye");
					shutdown.release();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected void log(String msg){
		System.out.println(">> "+msg);
	}
}
