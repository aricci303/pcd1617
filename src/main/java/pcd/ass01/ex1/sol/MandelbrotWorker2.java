package pcd.ass01.ex1.sol;

public class MandelbrotWorker2 extends Thread {

	private int nIterMax, xStart, xEnd;
	private MandelbrotSetImage image; 
	
	public MandelbrotWorker2(MandelbrotSetImage image, int nIterMax, int xStart, int xEnd){
		this.image = image;
		this.nIterMax = nIterMax;
		this.xStart = xStart;
		this.xEnd = xEnd;
	}
	
	public void run(){
		int x = xStart;
		log("Computing stripes from "+xStart+" "+xEnd);
		while (x < xEnd){
			for (int y = 0; y < image.getHeight(); y++){
				image.compute(x,y,nIterMax);
			}
			x++;
		}
		log("Done.");
	}
	
	protected void log(String msg){
		System.out.println("["+getName()+"] "+msg);
	}
}
