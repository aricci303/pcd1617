package pcd.lab07.actors.macrostepsem;

import akka.actor.UntypedActor;

public class UnstoppableActor extends UntypedActor {
	private boolean stopped;

	public void onReceive(Object msg) {
		if (msg instanceof PrintMsg) {
			stopped = false;
			PrintMsg m = (PrintMsg) msg;
			for (int i = 0; i < m.getNum() && !stopped; i++) {
				System.out.println(" " + i);				
			}
		} else if (msg instanceof StopMsg) {
			System.out.println("stopped!");
			stopped = true;
		} else {
			unhandled(msg);
		}
	}
}
