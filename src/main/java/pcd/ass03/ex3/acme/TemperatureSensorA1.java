package pcd.ass03.ex3.acme;


import pcd.ass03.ex3.TemperatureSensor;

/**
 * Class implementing a simulated temperature sensor
 * (Assignment #03)
 * 
 * @author aricci
 *
 */
public class TemperatureSensorA1 extends TempSensorImpl implements TemperatureSensor {	
	
	public TemperatureSensorA1(){
		super(5,25,1,0.05);
	}
}

