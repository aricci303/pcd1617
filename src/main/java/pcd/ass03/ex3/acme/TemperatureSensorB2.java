package pcd.ass03.ex3.acme;

import io.reactivex.Flowable;
import pcd.ass03.ex3.ObservableTemperatureSensor;

public class TemperatureSensorB2 extends ObservableTempSensorImpl implements ObservableTemperatureSensor {

	public TemperatureSensorB2() {
		super(10, 30, 1, 0.05);
	}

	public Flowable<Double> createObservable(){
		return createObservable(500);
	}
}
