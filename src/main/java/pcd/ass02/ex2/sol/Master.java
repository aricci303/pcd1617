package pcd.ass02.ex2.sol;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pcd.ass02.ex2.*;

public class Master extends Thread {

	private MandelbrotSetImage set;
	private MandelbrotView view;
	private StopFlag stopFlag;
	private int nTasks;
	
	public Master(MandelbrotSetImage set, MandelbrotView view,  StopFlag stopFlag, int nTasks){
		this.set = set;
		this.view = view;
		this.stopFlag = stopFlag;
		this.nTasks = nTasks;
	}
	
	public void run(){
		
		int poolSize = Runtime.getRuntime().availableProcessors()+1;

		int nPoints = set.getHeight()*set.getWidth()/nTasks;
		int rem = set.getHeight()*set.getWidth() % nTasks;
		int nIter = 500;
		int step = nTasks;
		int oldx0;

		StopWatch cron = new StopWatch();

		Complex c0 = new Complex(-0.75,0);
		double rad0 = 2;

		Complex c1 = new Complex(-0.75,0.1);
		Complex c2 = new Complex(-0.1011,0.9563);
		Complex c3 = new Complex(0.254,0);
		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);
		double radius = rad0;
		
		ExecutorService executor = Executors.newFixedThreadPool(poolSize);
		
		
		Set<Future<Void>> resultSet = new HashSet<Future<Void>>();
		while (!stopFlag.isSet()){			
			cron.start();
			resultSet.clear();
			int x0 = 0;
			int y0 = 0;
			for (int i=0; i< nTasks; i++) {
				try {
					Future<Void> res = executor.submit(new ComputeMandelbrotRegionTask(set, x0, y0, step, nPoints + (rem > i ? 1:0), nIter));
					resultSet.add(res);
					oldx0 = x0;
					x0 = (x0 + 1) % set.getWidth();
					if (x0 < oldx0){
						y0++;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}				

		    for (Future<Void> future : resultSet) {
		    	try {
		    		future.get();
		    	} catch (Exception ex){
		    		ex.printStackTrace();
		    	}
		    }
			view.repaint();
			cron.stop();
			System.out.println("Frame computed in "+cron.getTime()+" ms");
			/*
			try {
				Thread.sleep(5000);
			} catch (Exception ex){
				
			}*/
			radius *= 0.95;
			set.updateRadius(radius);
		}
		executor.shutdown();
	}

	private void log(String msg){
		// System.out.println("[TASK-"+this+"] "+msg);
	}
	
}
