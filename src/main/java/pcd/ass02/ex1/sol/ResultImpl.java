package pcd.ass02.ex1.sol;

import pcd.ass02.ex1.Result;

public class ResultImpl implements Result {
	
	private long value;
	
	public ResultImpl(long secretNumber, long guess){
		this.value = (secretNumber - guess);
	}
	
	@Override
	public boolean found() {
		return value == 0;
	}

	@Override
	public boolean isGreater() {
		return value > 0;
	}

	@Override
	public boolean isLess() {
		return value < 0;
	}

}
