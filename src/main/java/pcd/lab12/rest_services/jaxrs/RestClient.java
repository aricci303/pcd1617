package pcd.lab12.rest_services.jaxrs;

import static javax.ws.rs.client.Entity.json;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class RestClient {
    public static void main(String[] args) throws Exception {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        Server jettyServer = new Server(8081);

        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");

        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.classnames",
                MyConsumerService.class.getCanonicalName());

        try {
            jettyServer.start();

			Client client = ClientBuilder.newClient();
			WebTarget myRes = client.target("http://localhost:8080/myservice/requestsWithHook");

			JsonObject msg = Json.createObjectBuilder().add("input", 16)
					.add("resURI", "http://localhost:8081/myconsservice/results/myres").build();

			log("sending request: " + msg);

			/* sync, can be made async */
			
			Invocation req = myRes.request(MediaType.APPLICATION_JSON).buildPost(json(msg.toString()));
			String ret = req.invoke(String.class);

			log("service req ret " + ret);
            
            
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
    
	static public void log(String msg) {
		Logger.getGlobal().log(Level.INFO,"[REST CLIENT] " + msg);
	}
    
}
