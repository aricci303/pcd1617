package pcd.lab12.rest_services.vertx;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.*;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;


/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class MyService {

  public static void main(String[] args) {
	
	  Vertx vertx = Vertx.vertx();
	  Router router = Router.router(vertx);

	 
	  router.route("/").handler(routingContext -> {
	    HttpServerResponse response = routingContext.response();
	    response
	        .putHeader("content-type", "text/html")
	        .end("<h1>Entry point.</h1>");
	  });

	  router.get("/api/resources").handler(routingContext -> {
		  HttpServerRequest req = routingContext.request();
		  HttpServerResponse response = routingContext.response();
		  System.out.println(req.method().toString());
			response
			        .end("Reading res list");
	  });
	  
	  router.postWithRegex("\\/api\\/resources\\/id([0-9])+").handler(routingContext -> {
		  HttpServerRequest req = routingContext.request();
		  HttpServerResponse response = routingContext.response();
		  response
		        .end("creating res."+req.path());
	  });

	  router.putWithRegex("\\/api\\/resources\\/id([0-9])+").handler(routingContext -> {
		  HttpServerRequest req = routingContext.request();
		  HttpServerResponse response = routingContext.response();
		  response
		        .end("changing res."+req.path());
	  });

	  router.route("/assets/*").handler(StaticHandler.create("assets"));

	  // Create the HTTP server and pass the "accept" method to the request handler.
	  
	  State state = new State();
	  
	  vertx
	      .createHttpServer()
	      .websocketHandler(ws -> {
	      	  System.out.println("WebSocket opened!");
	      	  ws.handler(hnd -> {
		      	  System.out.println("data received: "+hnd.toString());
		      	  state.update();
		      	  Buffer buffer = Buffer.buffer().appendString("pong-"+state.getCount());
		      	  ws.write(buffer);
	      	  });
	          // if (req.uri().equals("/")) req.response().sendFile(path+"/ws.html");
	        })
		  .requestHandler(router::accept)
	      .listen(8080);
  } 
  
}
